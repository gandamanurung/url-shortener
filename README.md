# URL Shortener

This URL Shortener a simple URL shortener built using PHP Slim Framework. User interface is built using jQuery, Materialize CSS, and HTML 5

## Getting Started

### System Requirements

Please configure Apache, PHP and MySQL into your machine first or installing [XAMPP](https://www.apachefriends.org/index.html). XAMPP version 1.7.7 will be enough.

### Installation

Restore urlshortener.sql to your MySQL database.

Change database configuration in config.php with your environment configuration.

```sh
$dbhost="MYSQL_HOST";
$dbuser="MYSQL_USERNAME";
$dbpass="MYSQL_PASSWORD";
$dbname="urlshortener";
```

Find this snippet in index.php
```sh
var shortenURL = 'http://localhost/urlshortener/shorten';
```

Changes localhost to your host IP or hostname.

Execute the application from your browser.
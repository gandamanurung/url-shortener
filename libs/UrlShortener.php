<?php

class UrlShortener 
{
    const BASE = 10;
    const ENCODE_BASE = 64;
    const SHORT_BASE_URL = 'http://sho.rt/'; //Assume as they are
    private static $base_alphabet = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-';

    public static function encode($record_id) 
    {
        $shorten_url = '';
        while($record_id>0) 
        {
            $remainder = $record_id % self::ENCODE_BASE;
            $record_id = ($record_id-$remainder) / self::ENCODE_BASE;     
            $shorten_url = self::$base_alphabet{$remainder} . $shorten_url;
        }
        return self::SHORT_BASE_URL . $shorten_url;
    }
}

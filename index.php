<?php
/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 */
require 'Slim/Slim.php';
require 'libs/UrlShortener.php';
require 'config.php';

\Slim\Slim::registerAutoloader();

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim();

/**
 * Step 3: Define the Slim application routes
 *
 * Here we define several Slim application routes that respond
 * to appropriate HTTP request methods. In this example, the second
 * argument for `Slim::get`, `Slim::post`, `Slim::put`, `Slim::patch`, and `Slim::delete`
 * is an anonymous function.
 */

// GET route
$app->get(
    '/',
    function () {
        $template = <<<EOT
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
  <title>URL Shortener</title>

  <!-- CSS  -->
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
  <nav class="light-blue lighten-1" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="#" class="brand-logo">URL Shortener</a>

      <ul id="nav-mobile" class="side-nav">
        
      </ul>
      <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
    </div>
  </nav>
  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <h1 class="header center orange-text">Shorten your URL!</h1>
      <div class="row center">
        <input type="text" name="long_url" id="long_url" placeholder="Your long URL" />
      </div>
      <div id="message_wrapper">
        
      </div>
      <br><br>
      <div class="row center">
        <a href="javascript:void(0)" id="addurl-button" class="btn-large waves-effect waves-light orange">Get it Done!</a>
      </div>
      <br><br>
<br><br><br><br><br>
    </div>
  </div>




  <footer class="page-footer orange">
    <div class="footer-copyright">
      <div class="container">
      CSS by <a class="orange-text text-lighten-3" href="http://materializecss.com">Materialize</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>

  <script type="text/javascript">
    var shortenURL = 'http://localhost/urlshortener/shorten';
    $( document ).ready(function() 
    {
        $( "#addurl-button" ).click(function() 
        {
          shortedAnURL();
        });

        $('#long_url').bind("enterKey",function(e){
           shortedAnURL();
        });

        $('#long_url').keyup(function(e){
            if(e.keyCode == 13)
            {
                $(this).trigger("enterKey");
            }
        });

    });

    
    function fillErrorMessage(errorString)
    {
        var message = '<p class="message" id="ErrorMessage"><i class="mdi-alert-error"></i>'+errorString+'</p>';
        $('#message_wrapper').html(message);
    }

    function fillSuccessMessage(successString)
    {
        var message = '<p class="message" id="SuccessMessage">'+successString+'</p>';
        $('#message_wrapper').html(message);
    }

    function clearMessageWrapper()
    {
        $('#message_wrapper').html('');
    }

    function isValidURL(url)
    {
      // from http://blog.roymj.co.in/url-validation-using-regular-expression-javascript/
      var myRegExp =/^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$/i;
      return myRegExp.test(url);
    }

    function shortedAnURL() 
    {
        var url = $("#long_url").val();

        clearMessageWrapper();

        if(url.trim() == "")
        {
            fillErrorMessage('Oops! Looks like you forgot to place your long URL.');
        }
        else if(!isValidURL(url))
        {
            fillErrorMessage('Oops! Looks like your long URL is invalid.');
        }
        else
        {
            $('#addurl-button').addClass('disabled');

            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: shortenURL,
                dataType: "json",
                data: formToJSON(),
                success: function(data, textStatus, jqXHR)
                {
                    fillSuccessMessage('Shorten URL: <a href="' + data.short_url + '">' +  data.short_url + '</a>');
                    $('#addurl-button').removeClass('disabled');
                },
                error: function(jqXHR, textStatus, errorThrown)
                {
                    fillErrorMessage('Error: '+ textStatus)
                    $('#addurl-button').removeClass('disabled');
                }
            });
        }
    }

    // Helper function to serialize all the form fields into a JSON string
    function formToJSON() 
    {
        return JSON.stringify({
            "long_url": $('#long_url').val(),
            });
    }

  </script>

  </body>
</html>


EOT;
        echo $template;
    }
);

$app->post('/shorten', 'addNewURL');
/**
 * Step 4: Run the Slim application
 *
 * This method should be called last. This executes the Slim application
 * and returns the HTTP response to the HTTP client.
 */
$app->run();

function addNewURL() 
{
    $request = \Slim\Slim::getInstance()->request();
    $data = json_decode($request->getBody());


    $query_sql = "SELECT * FROM urls WHERE long_url=:long_url";
    try 
    {
        $db = getConnection();
        $stmt = $db->prepare($query_sql);
        $stmt->bindParam("long_url", $data->long_url);
        $stmt->execute();
        $shorten_url = $stmt->fetchObject();

        if(!empty($shorten_url))
        {
            echo json_encode($shorten_url);
            exit;
        }
    } 
    catch(PDOException $e) 
    {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }

    $insert_sql = "INSERT INTO urls (long_url) VALUES (:long_url)";

    $shorten_url = new stdClass;


    try
    {
        $db = getConnection();
        $stmt = $db->prepare($insert_sql);
        $stmt->bindParam("long_url", $data->long_url);
        $stmt->execute();

        $shorten_url->id = $db->lastInsertId();
        $shorten_url->long_url = $data->long_url; 

    } 
    catch(PDOException $e) 
    {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }

    $shorten_url->short_url = UrlShortener::encode($shorten_url->id);

    $update_sql = "UPDATE urls SET short_url=:short_url WHERE id=:id";
    try 
    {
        $db = getConnection();
        $stmt = $db->prepare($update_sql);
        $stmt->bindParam("short_url", $shorten_url->short_url);
        $stmt->bindParam("id", $shorten_url->id);
        $stmt->execute();
        $db = null;
        echo json_encode($shorten_url);
    } 
    catch(PDOException $e) 
    {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}
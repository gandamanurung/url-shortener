<?php
    $dbhost="localhost";
    $dbuser="root";
    $dbpass="";
    $dbname="urlshortener";

    function getConnection() 
    {
        global $dbhost, $dbuser, $dbpass, $dbname;
        $pdo = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);  
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $pdo;
    }
